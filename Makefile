all: compile
dev: compile check test

clean:
	rebar clean

test:
	rebar eunit

compile:
	rebar compile

check:
	dialyzer -r ebin

build_plt:
	dialyzer --build_plt --apps erts kernel stdlib
