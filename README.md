**Ecoro** it's implementation pseudo-coroutines like [Lua-coroutines](http://www.lua.org/pil/9.1.html) for Erlang.

# Examples

## Simple ##

~~~
%% create coroutine
Coro = ecoro:start(fun(State0) ->
	io:format("~p~n", [State0]),
	State1 = ecoro:yield(State0 + 1),
	
	io:format("~p~n", [State1]),
	State2 = ecoro:yield(State1 + 1),
	
	io:format("~p~n", [State2]),
	State2 + 1
end),

%% start coroutine (print 0 and return true because coro is alive)
{true,  State1 = 1} = ecoro:resume(Coro, 0),

%% resume coroutine (print 1)
{true,  State2 = 2} = ecoro:resume(Coro, State1),

%% resume coroutine (print 2 and retur false because coro is shutdown)
{false, _State3 = 3} = ecoro:resume(Coro, State2).
~~~

## Error handling ##

~~~
%% create bad coroutine
Coro = ecoro:start(fun(_) ->
	io:format("1~n"),
	ecoro:yield(),
	erlang:error(some_error)
end),

%% start coroutine (print 1 and return true because coro is alive)
{true, _} = ecoro:resume(Coro),

%% resume coroutine (return error with error reason)
{error, {error, some_error}} = ecoro:resume(Coro).
~~~