-module(ecoro).

-export([wrap/1]).
-export([start/1]).
-export([resume/1, resume/2]).
-export([yield/0, yield/1]).
-export([shutdown/1]).
-export([is_dead/1]).

-type ecoro_arg()        :: any().
-type ecoro_func()       :: fun((ecoro_arg()) -> ecoro_arg()).
-type ecoro_wrap_func()  :: fun((ecoro_arg()) -> ecoro_resume_ret()).
-type ecoro_resume_ret() :: {boolean(), ecoro_arg()} | {error, any()}.

-export_type([
	ecoro_arg/0, ecoro_func/0,
	ecoro_wrap_func/0, ecoro_resume_ret/0]).

-define(PROC_DICT_PARENT, ecoro_proc_dict_parent).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% wrap
%% ------------------------------------

-spec wrap(
	ecoro_func()
) -> ecoro_wrap_func().

wrap(Func) ->
	Coro = start(Func),
	fun(Arg) -> resume(Coro, Arg) end.

%% ------------------------------------
%% start
%% ------------------------------------

-spec start(
	ecoro_func()
) -> pid().

start(Func) ->
	Self = self(),
	erlang:spawn(fun() ->
		Arg = wait_resume(),
		try
			Rets = Func(Arg),
			Self ! {self(), ecoro_dead, Rets}
		catch Exception:Reason ->
			Self ! {self(), ecoro_error, {Exception, Reason}}
		end
	end).

%% ------------------------------------
%% resume
%% ------------------------------------

-spec resume(
	pid()
) -> ecoro_resume_ret().

resume(Coro) ->
	resume(Coro, undefined).

-spec resume(
	pid(), ecoro_arg()
) -> ecoro_resume_ret().

resume(Coro, Arg) ->
	false = is_dead(Coro),
	false = self() == Coro,
	Coro ! {self(), ecoro_resume, Arg},
	receive
		{Coro, ecoro_yield, Ret} -> {true,  Ret};
		{Coro, ecoro_dead,  Ret} -> {false, Ret};
		{Coro, ecoro_error, Err} -> {error, Err}
	end.

%% ------------------------------------
%% yield
%% ------------------------------------

-spec yield(
) -> ecoro_arg().

yield() ->
	yield(undefined).

-spec yield(
	ecoro_arg()
) -> ecoro_arg().

yield(Arg) ->
	Parent = erlang:get(?PROC_DICT_PARENT),
	false  = Parent == undefined,
	Parent ! {self(), ecoro_yield, Arg},
	wait_resume().

%% ------------------------------------
%% shutdown
%% ------------------------------------

-spec shutdown(
	pid()
) -> ok.

shutdown(Coro) ->
	false = is_dead(Coro),
	false = self() == Coro,
	Coro ! {self(), ecoro_shutdown},
	receive
		{Coro, ecoro_shutdown} -> ok
	end.

%% ------------------------------------
%% is_dead
%% ------------------------------------

-spec is_dead(
	pid()
) -> boolean().

is_dead(Coro) ->
	false = self() == Coro,
	not erlang:is_process_alive(Coro).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% wait_resume
%% ------------------------------------

-spec wait_resume(
) -> ecoro_arg().

wait_resume() ->
	receive
		{Parent, ecoro_resume, Arg} ->
			erlang:put(?PROC_DICT_PARENT, Parent),
			Arg;
		{Parent, ecoro_shutdown} ->
			Parent ! {self(), ecoro_shutdown},
			erlang:exit(self(), shutdown)
	end.

%% ----------------------------------------------------------------------------
%%
%% TESTS
%%
%% ----------------------------------------------------------------------------

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
test01_test() ->
	Coro = start(fun(1) ->
		2
	end),
	false      = is_dead(Coro),
	{false, 2} = resume(Coro, 1),
	true       = is_dead(Coro).

test02_test() ->
	Coro = start(fun(1) ->
		3 = yield(2),
		4
	end),
	false      = is_dead(Coro),
	{true,  2} = resume(Coro, 1),
	false      = is_dead(Coro),
	{false, 4} = resume(Coro, 3),
	true       = is_dead(Coro).

test03_test() ->
	Coro = start(fun(1) ->
		3 = yield(2),
		5 = yield(4),
		6
	end),
	false      = is_dead(Coro),
	{true,  2} = resume(Coro, 1),
	false      = is_dead(Coro),
	{true,  4} = resume(Coro, 3),
	false      = is_dead(Coro),
	{false, 6} = resume(Coro, 5),
	true       = is_dead(Coro).

test04_test() ->
	Coro = start(fun(1) ->
		3 = yield(2)
	end),
	false      = is_dead(Coro),
	{true,  2} = resume(Coro, 1),
	ok         = shutdown(Coro),
	true       = is_dead(Coro).

test05_test() ->
	WrapFunc = wrap(fun(1) ->
		3 = yield(2)
	end),
	{true,  2} = WrapFunc(1),
	{false, 3} = WrapFunc(3).

test06_test() ->
	WrapFunc = wrap(fun(1) ->
		3 = yield(2),
		erlang:throw(some_reason)
	end),
	{true, 2}                     = WrapFunc(1),
	{error, {throw, some_reason}} = WrapFunc(3).

test07_test() ->
	Coro = start(fun(1) ->
		erlang:error(some_reason)
	end),
	false                         = is_dead(Coro),
	{error, {error, some_reason}} = resume(Coro, 1),
	true                          = is_dead(Coro).

test08_test() ->
	Coro = start(fun(1) ->
		erlang:throw(some_reason)
	end),
	false                         = is_dead(Coro),
	{error, {throw, some_reason}} = resume(Coro, 1),
	true                          = is_dead(Coro).

test09_test() ->
	Coro = start(fun(1) ->
		erlang:exit(some_reason)
	end),
	false                        = is_dead(Coro),
	{error, {exit, some_reason}} = resume(Coro, 1),
	true                         = is_dead(Coro).

test10_test() ->
	Coro = start(fun(1) ->
		1
	end),
	false                             = is_dead(Coro),
	{error, {error, function_clause}} = resume(Coro, 2),
	true                              = is_dead(Coro).

test11_test() ->
	Coro = start(fun(undefined) ->
		undefined = yield()
	end),
	{true,  undefined} = resume(Coro),
	{false, undefined} = resume(Coro).

test12_test() ->
	%% create coroutine
	Coro = ecoro:start(fun(State0) ->
		io:format("~p~n", [State0]),
		State1 = ecoro:yield(State0 + 1),
		
		io:format("~p~n", [State1]),
		State2 = ecoro:yield(State1 + 1),
		
		io:format("~p~n", [State2]),
		State2 + 1
	end),

	%% start coroutine (print 0 and return true because coro is alive)
	{true,  State1 = 1} = ecoro:resume(Coro, 0),

	%% resume coroutine (print 1)
	{true,  State2 = 2} = ecoro:resume(Coro, State1),

	%% resume coroutine (print 2 and retur false because coro is shutdown)
	{false, _State3 = 3} = ecoro:resume(Coro, State2).

test13_test() ->
	%% create bad coroutine
	Coro = ecoro:start(fun(_) ->
		io:format("1~n"),
		ecoro:yield(),
		erlang:error(some_error)
	end),

	%% start coroutine (print 1 and return true because coro is alive)
	{true, _} = ecoro:resume(Coro),

	%% resume coroutine (return error with error reason)
	{error, {error, some_error}} = ecoro:resume(Coro).
-endif.
